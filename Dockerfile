FROM node:10-alpine

RUN apk update && apk add --no-cache ca-certificates curl
ADD pipe .

RUN npm i
RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
