# Bitbucket Pipelines Pipe: Jira Transition

Transition [Jira](https://www.atlassian.com/software/jira) issue.
> ##### Only supports Jira Cloud. Does not support Jira Server (hosted)
## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/jira-transition:0.2.1
  variables:
    JIRA_BASE_URL: '<string>'
    JIRA_USER_EMAIL: '<string>'
    JIRA_API_TOKEN: '<string>'
    ISSUE: '<string>'
    TRANSITION: '<string>'
    # DEBUG: '<string>' # Optional.
```

## Variables

| Variable           | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| JIRA_BASE_URL (*) | URL of Jira instance. Example: `https://<yourdomain>.atlassian.net`  |
| JIRA_API_TOKEN (*) | **Access Token** for Authorization. Example: `HXe8DGg1iJd2AopzyxkFB7F2` ([How To](https://confluence.atlassian.com/cloud/api-tokens-938839638.html)) |
| JIRA_USER_EMAIL (*) | Email of the user for which **Access Token** was created for . Example: `human@example.com` |
| ISSUE (*) | String to extract Issue key of the issue to perform a transition on. Example: `BP-1`, `$BITBUCKET_BRANCH`
| TRANSITION (*)     | Transition name to apply to an issue. Example: `In Progress` |
| DEBUG	| Turn on extra debug information. Example: `pipe` |

_(*) = required variable._

## Prerequisites

To transition an issue in Jira, you need to create a Personal access token. You can follow the instructions [here](https://confluence.atlassian.com/cloud/api-tokens-938839638.html) to create one.

## Examples

Basic example:

```yaml
script:
  - pipe: atlassian/jira-transition:0.2.1
    variables:
      JIRA_BASE_URL: $JIRA_BASE_URL
      JIRA_USER_EMAIL: $JIRA_USER_EMAIL
      JIRA_API_TOKEN: $JIRA_API_TOKEN
      ISSUE: "BP-1"
      TRANSITION: "Done"
```

Extract issue key from branch name:

```yaml
script:
  - pipe: atlassian/jira-transition:0.2.1
    variables:
      JIRA_BASE_URL: $JIRA_BASE_URL
      JIRA_USER_EMAIL: $JIRA_USER_EMAIL
      JIRA_API_TOKEN: $JIRA_API_TOKEN
      ISSUE: $BITBUCKET_BRANCH
      TRANSITION: "Done"
```


## Support
If you'd like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you're reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce

## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

[community]: https://community.atlassian.com/t5/forums/postpage/choose-node/true/interaction-style/qanda?add-tags=bitbucket-pipelines,pipes,jira
