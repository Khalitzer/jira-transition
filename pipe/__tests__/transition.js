// const nock = require('nock')
const pipe = require('../index')
const { mocks, utils } = require('./helpers')

test('Simple happy path', async () => {
  utils.waitForRequest(mocks.Jira.listeners.getIssue)
  utils.waitForRequest(mocks.Jira.listeners.getIssueTransitions)
  utils.waitForRequest(mocks.Jira.listeners.transitionIssue)
  utils.waitForRequest(mocks.Jira.listeners.getMyself)

  // console.error('pending mocks: %j', nock.pendingMocks())

  await pipe({
    JIRA_API_TOKEN: 'api_token',
    JIRA_BASE_URL: 'https://example.com',
    JIRA_USER_EMAIL: 'theiruser@example.com',
    ISSUE: 'ISSUE-1',
    TRANSITION: 'To Do',
  })
})
